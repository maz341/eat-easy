

class ImagePath {
  //images route
  static const String imageDir = "assets/icons";

  //  Images
  static const String burgerPath = "$imageDir/burger.png";
  static const String pizzaPath = "$imageDir/pizza.png";
  static const String spaghettiPath = "$imageDir/spaghetti.png";
  static const String sandwichPath = "$imageDir/sandwich.png";
  static const String drinkPath = "$imageDir/drink.png";




// Network Images
  static const String avatarNetworkImage = "https://oodp.ca/media/tutor-8.jpg";
}
