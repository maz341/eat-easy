class Promotion {
  String? imageUrl, discountUpto, label, name;

  Promotion({this.imageUrl, this.discountUpto, this.label, this.name});
}
