# EatEasy (Flutter) - made for AltaML

EatEasy restaurant application is a one-stop solution for all food lovers. With a user-friendly interface, it allows users to explore a wide variety of food options, conveniently place orders, and enjoy a seamless dining experience. Whether you're craving a quick snack or a full-course meal, our app offers a range of features and functionalities to satisfy your cravings.

## Logo

<div align='center'>
  <img src='./assets/logo.png' width='400' alt='EatEasy Logo'>
</div>

## Features

- Login and Sign Up: Users can create accounts or log in using their credentials to access personalized features and saved information.
- User Profile: Each user has a dedicated profile section where they can view and manage their personal details, order history, and payment information.
- Menu Categories: Foods are categorized into different sections, making it easy for users to browse and find their desired dishes quickly.
- Food Details: Users can view comprehensive food details including images, descriptions, prices, ingredients, and customer reviews to make informed choices.
- Cart Functionality: Users can add multiple food items to their cart, adjust quantities, and view the total cost before proceeding to check out.
- Order Placement: Seamless order placement process, allowing users to select delivery options, provide delivery addresses (using geo-location or manual entry), and choose payment methods.
- Nearby Restaurants: The app uses geo-location to identify and display nearby restaurants, making it convenient to explore local dining options.
- Promotional Offers: Regularly updated promotions and discounts are showcased in a dedicated section to provide users with attractive deals.
- Reviews and Ratings: Users can read and provide feedback on food items, helping others make informed decisions while browsing the menu.
- Address Management: Users can save and manage multiple delivery addresses for quick and easy order placement.
- Geo Locator: Integration with geo-location services enables users to automatically detect and populate their current location for precise delivery.
- User Notifications: Instant updates and notifications are sent to users regarding order status, delivery updates, and promotional offers.
- Social Sharing: Users can share their favorite food items, reviews, and restaurant experiences with friends and family via social media platforms.

## 🛠 Skills

Flutter, Android and iOS

## Demo

<div align='center'>
  <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/1.jpg' width='200' alt='Screenshot 1'>
  <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/2.jpg' width='200' alt='Screenshot 2'>
  <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/3.jpg' width='200' alt='Screenshot 3'>
 
</div>
<div align='center'>

 <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/4.jpg' width='200' alt='Screenshot 4'>
  <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/5.jpg' width='200' alt='Screenshot 5'>
  <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/6.jpg' width='200' alt='Screenshot 6'>
</div>

<div align='center'>

 <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/7.jpg' width='200' alt='Screenshot 4'>
  <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/8.jpg' width='200' alt='Screenshot 5'>
  <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/9.jpg' width='200' alt='Screenshot 6'>
</div>

<div align='center'>

 <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/10.jpg' width='200' alt='Screenshot 4'>
  <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/11.jpg' width='200' alt='Screenshot 5'>
  <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/12.jpg' width='200' alt='Screenshot 6'>
</div>

<div align='center'>

 <img src='https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/13.jpg' width='200' alt='Screenshot 4'>
  
</div>
#### Video Demo:

[Video Link](https://gitlab.com/maz341/eat-easy/-/blob/main/assets/screenshots/recording.mp4)

![Video Demo](https://gitlab.com/maz341/eat-easy/-/raw/main/assets/screenshots/recording.mp4)

## 🔗 Links

[![Website](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://maazkamal.com)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/mazkamal/)
[![Gitlab](https://img.shields.io/badge/gitlab-fc6d27?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/maz341)
